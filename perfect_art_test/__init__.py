from enum import Enum


class AdditionStrategy(Enum):
    COPY_LIST = 1
    MAP_IN_PLACE = 2


def addition(span, strategy=AdditionStrategy.COPY_LIST):

    def _map_in_place(fn, l):
        for i, v in enumerate(l):
            l[i] = fn(i, v)
        return l

    def _addition_func(index, value, addend):
        return value + addend if (index +1) % span == 0 else value

    if strategy is AdditionStrategy.MAP_IN_PLACE:
        return lambda addend, values: _map_in_place(lambda index, value: _addition_func(index, value, addend),
                                                   values)
    if strategy is AdditionStrategy.COPY_LIST:
        return lambda addend, values: [_addition_func(index, value, addend)
                                       for index, value in enumerate(values)]
    assert 'Please specify correct addition strategy'

