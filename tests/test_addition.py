from unittest import TestCase

from perfect_art_test import AdditionStrategy, addition


class TestAddition(TestCase):
    def test_addition(self):
        for strategy in AdditionStrategy:
            with self.subTest(strategy=strategy):
                add2 = addition(2, strategy)
                result = add2(3, [1, 2, 3, 4, 5])
                self.assertEqual([1, 5, 3, 7, 5], result)

                result = add2(8, [1, 2, 3, 4, 5])
                self.assertEqual([1, 10, 3, 12, 5], result)

                add5 = addition(5, strategy)
                result = add5(-3, [1, 2, 3, 4, 5])
                self.assertEqual([1, 2, 3, 4, 2], result)

                add1 = addition(1, strategy)
                result = add1(1, [1, 2])
                self.assertEqual([2, 3], result)

    def test_addition_map_list_is_same_object_return(self):
        add2 = addition(2, AdditionStrategy.MAP_IN_PLACE)
        values = [1, 2, 3, 4, 5]
        result = add2(3, values)
        self.assertIs(values, result)
